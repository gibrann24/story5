from django.db import models
from django.forms import ModelForm

#Create your models here.
class ClassYear(models.Model):
    YEAR_CHOICES= [('2019', '2019'),
    ('2018', '2018'),
    ('2017', '2017'),
    ('2016', '2016'),
    ('Others', 'Others')]
    year = models.CharField(max_length=255, choices=YEAR_CHOICES, default='NULL')

    def __str__(self):
        return self.year

class Friends(models.Model):
    name = models.CharField(max_length=255, primary_key=True)
    ClassYear = models.ForeignKey(ClassYear, on_delete=models.CASCADE, default='Fasilkom')
    hobby = models.CharField(max_length=255)
    food = models.CharField(max_length=255)
    drink = models.CharField(max_length=255)


    def __str__(self):
        return self.name

