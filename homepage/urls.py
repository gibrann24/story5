from django.urls import path
from . import views
from django.contrib import admin

app_name = 'homepage'

urlpatterns = [
    path('', views.index, name='index'),
    path('seefriend/', views.seefriend, name='seefriend'),
    path('addfriend/', views.addfriend, name='addfriend'),

    # dilanjutkan ...
]